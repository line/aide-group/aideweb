{ # Ref: https://gyp.gsrc.io/index.md
  "targets": [{
   "target_name": "wrapper",
    "sources": [ 
      "src/WrapperCCService.cpp",
      "src/wrap.cpp"
    ],
    "cflags_cc": [ "-Wno-psabi", "-fexceptions", "-Wno-varargs" ],
    "cflags_cc!": [ "-fno-exceptions", "-fno-rtti", "-Wvarargs" ],
    "defines": [ "ON_GYP", "NAPI_DISABLE_CPP_EXCEPTIONS" ],
    "include_dirs": [
      "./src",
      "<!@(node -p \"require('node-addon-api').include\")"
    ],
    "library_dirs": [
    ],
    "libraries": [
    ]
  }]
}

