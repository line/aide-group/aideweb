# aideweb

Javascript server side node express utilities for web applications and service

@aideAPI

This package allows to compile and link C/C++ source code with a Javascript express web server and use it as a web service.

<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aideweb/'>https://gitlab.inria.fr/line/aide-group/aideweb/</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aideweb'>https://line.gitlabpages.inria.fr/aide-group/aideweb</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/aideweb//-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/aideweb//-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/aideweb/'>softwareherirage.org</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/aideweb/.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage


In order to test the package, run `make test -i` in the `./src` directory and observe the web page.

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>bindings: <a target='_blank' href='https://github.com/TooTallNate/node-bindings'>Helper module for loading your native module's .node file</a></tt>
- <tt>express: <a target='_blank' href='http://expressjs.com/'>Fast, unopinionated, minimalist web framework</a></tt>
- <tt>express-fileupload: <a target='_blank' href='https://www.npmjs.com/package/express-fileupload'>Simple express file upload middleware that wraps around Busboy</a></tt>
- <tt>node-addon-api: <a target='_blank' href='https://github.com/nodejs/node-addon-api'>Node.js API (Node-API)</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Authors

- Lola Denet&nbsp; <big><a target='_blank' href='mailto:lola.denet@etu.u-bordeaux.fr'>&#128386;</a></big>&nbsp; <big><a target='_blank' href='https://www.linkedin.com/in/lola-denet-bioinfo'>&#128463;</a></big>
- Thierry Viéville&nbsp; <big><a target='_blank' href='mailto:thierry.vieville@inria.fr'>&#128386;</a></big>
